use image::{GenericImageView, imageops};
use std::{fs, env, path, cmp};
use crossbeam::thread;
use std::ffi::OsStr;

fn get_extension_from_filename(filename: &str) -> Option<&str> {
    path::Path::new(filename)
        .extension()
        .and_then(OsStr::to_str)
}

fn length(n: u32, base: u32) -> u8 {
    let mut power = base;
    let mut count = 1;
    while n >= power {
        count += 1;
        if let Some(new_power) = power.checked_mul(base) {
            power = new_power;
        } else {
            break;
        }
    }
    count
}

fn main() {

    // parse and check input args
    let args: Vec<String> = env::args().collect();
    let argsourcepath = &args[1];
    let argcropsize = &args[2];
    let argoutpath = &args[3];


    // check if source file exist
    if !(path::Path::new(argsourcepath).exists()) {
        eprintln!("{} does not exist !",argsourcepath);
        return;
    }
    let ext = get_extension_from_filename(argsourcepath).unwrap();        

    // check if cropsize is an integer
    let cropsize: u32 = match argcropsize.parse() {
    Ok(n) => {
        n
    },
    Err(_) => {
        eprintln!("error: second argument not an integer");
        return;
    },
    };

    // make sure a `crop/` dir exists to put images into
    fs::create_dir_all(argoutpath).ok();

    // The dimensions method returns the images width and height.
    let img = image::open(argsourcepath).unwrap();
    let (x,y) = img.dimensions();
    
    // get number of decimals of highest dimension to pad at the end
    let num_dec = cmp::max(length(x, 10), length(y,10)) as usize;

    for dx in (0..x).step_by(cropsize.try_into().unwrap()) {
        thread::scope(|s| {
            s.spawn(|_| {
        for dy in (0..y).step_by(cropsize.try_into().unwrap()) {

            // check that the crop does not go beyond image border
            let mut dxf = dx;
            let mut dyf = dy;
            if dx + cropsize > x {dxf = x - cropsize;}
            if dy + cropsize > y {dyf = y - cropsize;}
            
            println!("{:?},{:?}", dxf,dyf);

            // crop image
            let sub_img = imageops::crop_imm(&img, dxf, dyf, cropsize, cropsize);
            // save crop
            let path = format!("{}{}_{}.{}", 
                               argoutpath, 
                               // pad with zeros for clean sorting
                               format!("{:0width$}",dxf, width = num_dec), 
                               format!("{:0width$}",dyf, width = num_dec),
                               ext);
            sub_img.to_image()
                   .save(&path)
                   .unwrap();

        }
            });
        }).unwrap();
    }
}
