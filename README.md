# A programm to crop images written in rust

## Compilation

```
cargo build
```

or alternatively,

```
cargo run test.png 256 crop/
```

## Usage

```
crop source_image_path cropsize output_dir_path
```

this will output square crops of the original image into the output dir with the naming convention `topleftx_toplefty.original_file_extension`. The format of the image is guessed by the `image` crate during saving. Suppported format are those supported by the crate in the version used here: `image = "0.24.3"`
